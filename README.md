# Log

Log provides a simplistic implementation for a log agent for service-based go programs.

It writes to STDOUT and STDERR

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/bf86/lib/go/log.svg)](https://pkg.go.dev/gitlab.com/bf86/lib/go/log)

### Middleware hook

There is a middleware hook that _should_ work with the standard http library, as well as [gorilla/mux]("github.com/gorilla/mux")