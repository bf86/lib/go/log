package log

import (
	"fmt"
	"net/http"

	"github.com/google/uuid"
)

type ResponseLogger struct {
	w    http.ResponseWriter
	Code int
}

// Write function overwrites the http.ResponseWriter Write() function
func (rl *ResponseLogger) Write(buf []byte) (int, error) {
	return rl.w.Write(buf)
}

// Header function overwrites the http.ResponseWriter Header() function
func (rl *ResponseLogger) Header() http.Header {
	return rl.w.Header()

}

// WriteHeader function overwrites the http.ResponseWriter WriteHeader() function
func (rl *ResponseLogger) WriteHeader(code int) {
	rl.Code = code
	rl.w.WriteHeader(code)
}

// NewResponseLogger creates a new readable response wrapper
func NewResponseLogger(w http.ResponseWriter) *ResponseLogger {
	return &ResponseLogger{
		w:    w,
		Code: 200,
	}
}

// Middleware logs each API Request
func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		requestId := uuid.NewString()
		Info(fmt.Sprintf("%s %s", req.Method, req.URL.Path), map[string]interface{}{
			"request": map[string]interface{}{
				"id":     requestId,
				"uri":    req.URL.Path,
				"method": req.Method,
			},
		})
		response := NewResponseLogger(w)

		next.ServeHTTP(response, req)

		Info(fmt.Sprintf("%d %s", response.Code, http.StatusText(response.Code)), map[string]interface{}{
			"request": map[string]interface{}{
				"id": requestId,
			},
			"response": map[string]interface{}{
				"code":   response.Code,
				"status": http.StatusText(response.Code),
			},
		})
	})
}
