package log

import (
	"encoding/json"
	"os"
	"time"
)

type LogLevel string

var log *Log

const logInfo LogLevel = "info"
const logWarning LogLevel = "warn"
const logError LogLevel = "warn"

// Log standardises logging from the API
type Log struct {
	Timestamp int64                  `json:"timestamp"`
	Level     LogLevel               `json:"level"`
	Message   string                 `json:"message,omitempty"`
	Metadata  map[string]interface{} `json:"-"`
	Fields    Fields                 `json:"fields,omitempty"`
}

// Fields is a map[string]interface made available to add additional logging fields
type Fields map[string]interface{}

// LogRequest is a Log, but separated for extensibility
type LogRequest struct {
	Log
}

// LogResponse is a Log, but separated for extensibility
type LogResponse struct {
	Log
}

func init() {
	log = new(Log)
	log.Timestamp = time.Now().Unix()
	log.Level = logInfo
	log.Metadata = map[string]interface{}{}
	log.Fields = map[string]interface{}{}
}

func write(log *Log, out *os.File) {
	if out == nil {
		out = os.Stdout
	}
	json.NewEncoder(out).Encode(log)
}

func addFields(meta ...Fields) {
	for _, data := range meta {
		for key := range data {
			log.Fields[key] = data[key]
		}
	}
}

func AddPersistentMetadata(key string, value interface{}) {
	log.Metadata[key] = value
}

func Logger() *Log {
	return log
}

func Info(message string, meta ...Fields) {
	log.Fields = Fields{} // reset fields per call
	log.Level = logInfo
	log.Timestamp = time.Now().Unix()
	log.Message = message
	addFields(log.Metadata)
	addFields(meta...)

	write(log, os.Stdout)
}

func Warn(message string, meta ...Fields) {
	log.Fields = Fields{} // reset fields per call
	log.Level = logWarning
	log.Timestamp = time.Now().Unix()
	log.Message = message
	addFields(log.Metadata)
	addFields(meta...)

	write(log, os.Stdout)
}

func Error(message string, meta ...Fields) {
	log.Fields = Fields{} // reset fields per call
	log.Level = logError
	log.Timestamp = time.Now().Unix()
	log.Message = message
	addFields(log.Metadata)
	addFields(meta...)

	write(log, os.Stderr)
}
